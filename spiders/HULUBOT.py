import scrapy
from ..items import ProviderMovie, ProviderShow
from ..hulu_functions import movies_offers, movies_availabilites, movies, show_offers
from ..hulu_functions import show_availabilities, unique_seasons, get_seasons, shows
import json

class HulubotSpider(scrapy.Spider):


    name = 'HULUBOT'
    start_urls = ['https://www.hulu.com/']



    def parse(self, response):
        """
        This function is the first one to start when we run the scrapy crawl.
        It runs on the first page and one by one makes request for the inner
        pages.
        These are all the second level inner pages in the HULU website from
        where we have to get our data
        """


        sections = ['/hub/originals',
                    '/hub/tv', 
                    '/hub/movies', 
                    '/hub/kids',
                    '/hub/networks']

        #sections = ['/hub/kids']
        # This loop makes request to each inner page.
        for sec in sections:
            URL = "https://www.hulu.com" + sec
            # List down scrappy exceptions and identify
            try:

                """
                As the networks page requires 3 level scrapping, 
                so this condition has to be applied
                """

                if sec == '/hub/networks':
                    post = scrapy.Request(URL, callback=self.net_pre1)
                else:
                    post = scrapy.Request(URL, callback=self.parse_pre1)
                yield post
            except:
                pass


    def net_pre1(self, response):
        """
        This function works on the first level of networks page
        """

        headings = response.css("div.Tile__content a::attr(href)").extract()
        # Generating request for all the headings picked up
        for heading in headings:
            URL = "https://www.hulu.com" + heading
            try:
                post = scrapy.Request(URL, callback=self.networks_page)
                yield post
            except:
                pass


    def parse_pre1(self, response):
        """
        This function works on the first level of all inner pages
        other than networks
        """

        # Picking up the nested headings within this page
        headings = response.css("div.Tile__content a::attr(href)").extract()
        # Generating request for all the headings picked up
        for heading in headings:
            URL = "https://www.hulu.com" + heading
            try:
                post = scrapy.Request(URL, callback=self.new_func)
                yield post
            except:
                pass

    def networks_page(self, response):
        """
        This function works on the intermediary networks page and picks
        the required headings to take tags out from
        """

        URLs = response.css('a.Tile__title-link::attr(href)').extract()
        for url in URLs:
            url = 'https://www.hulu.com' + url
            try:
                post = scrapy.Request(url, callback=self.new_func)
                yield post
            except:
                pass


    def new_func(self,response):
        """
        This function is responsible to picking up the tags from the scripts online
        and normalizing, cleaning, and validating the tags and data accordingly
        """

        print('Starting the bot')
        Things2 = response.css("#__NEXT_DATA__ ::text").extract_first()
        json_response2 = json.loads(Things2)

        HuluDict =[]

        if 'movie' in json_response2['query']['entity']:
            """ 
            Here we extract the tags for movies
            """
            print('Starting the bot for movies')

            Things = response.css("script[type = 'application/ld+json'] ::text").extract_first()
            json_response = json.loads(Things)
            OfferLinksDict = movies_offers(json_response)
            Availabilities = movies_availabilites(json_response, OfferLinksDict)
            Movies = movies(json_response,json_response2,Availabilities)
            HuluItems = ProviderMovie(Movies)
            HuluDict = HuluItems.to_native()
            HuluDict['ModelType'] = 'ProviderMovie'

        else:
            """ 
            Here we extract the tags for TV series
            """
            print('Starting the bot for series')

            Things = response.css("script[type = 'application/ld+json'] ::text").extract_first()
            json_response = json.loads(Things)
            OfferLinksDict = show_offers(json_response)
            Availabilities = show_availabilities(json_response, OfferLinksDict)
            epss = json_response2['props']['pageProps']['layout']['components'][3]['tabs'][0]['model']['collection'][
                'items']
            SeasonListUnique = unique_seasons(epss)
            SeasonList = get_seasons(SeasonListUnique, json_response, epss, response, Availabilities)
            Show = shows(json_response, json_response2, Availabilities, SeasonList)
            HuluItems = ProviderShow(Show)
            HuluDict = HuluItems.to_native()
            HuluDict['ModelType'] = 'ProviderShow'

        yield HuluDict


