# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface

from scrapy.exporters import JsonItemExporter
import datetime
from schematics.exceptions import ValidationError, DataError
from scrapy.exceptions import DropItem
from HULU.items import ProviderMovie, ProviderShow

class HuluPipeline:


    def process_item(self, item, spider):
        return item

class SchematicsValidationPipeline(object):

    STAT_FMT = 'schematics/errors/{field}'

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.stats)

    def __init__(self, stats):
        self.stats = stats

    def process_item(self, item, spider):
        errors = {}
        try:
            if item['ModelType'] == 'ProviderMovie':
                del item['ModelType']
                ProviderMovie(item).validate()
                item = ProviderMovie(item).to_primitive()
                item['ModelType'] = 'ProviderMovie'
            elif item['ModelType'] == 'ProviderShow':
                del item['ModelType']
                ProviderShow(item).validate()
                item = ProviderShow(item).to_primitive()
                item['ModelType'] = 'ProviderShow'
            if item['Source'] not in ['Tubi']:
                del item['Languages']
        except (ValidationError, DataError) as e:
            errors = e.messages
        fields_messages = []
        for field_name, err_msgs in errors.items():
            self.stats.inc_value(self.STAT_FMT.format(field=field_name))
            fields_messages.append((field_name, err_msgs))
        if errors:
            error_msg = ''
            for field_name, message in fields_messages:
                error_msg += u'{}: {}\n'.format(field_name, message)
            raise DropItem(u'schema validation failed: \n {}'
                           .format(error_msg))
        return item


class ModelTypeExportPipeline(object):
    def open_spider(self, spider):
        self.model_types_to_export = {}

    def close_spider(self, spider):
        for exporter in self.model_types_to_export.values():
            exporter.finish_exporting()

    def _exporter_for_item(self, item):
        model_type = item['ModelType']
        del item['ModelType']
        if model_type not in self.model_types_to_export:
            f = open('C:/crawler/output/{}/{}/{}.json'.format(model_type, item['Source'], datetime.datetime.utcnow().strftime("%Y-%m-%d")), 'wb')
            exporter = JsonItemExporter(f)
            exporter.start_exporting()
            self.model_types_to_export[model_type] = exporter
        return self.model_types_to_export[model_type]

    def process_item(self, item, spider):
        exporter = self._exporter_for_item(item)
        exporter.export_item(item)
        return item

