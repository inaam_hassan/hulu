# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import re
from schematics.models import Model
from schematics.types import BaseType, StringType, DecimalType, DateTimeType, BooleanType, ListType, ModelType, IntType, LongType

class IMDbId(BaseType):
    def to_primitive(self, value, content=None):
        if re.search(r't(\d{7,9})', value):
            return "tt" + re.search(r't(\d{7,9})', value).group(1)
        elif re.search(r'^(\d{7,9})$', value):
            return "tt" + re.search(r'^(\d{7,9})$', value).group(1)
        else:
            return None

class OfferLink(Model):
    Url = StringType(required=True)
    Platform = StringType(choices=["NOT_SET", "IOS", "Android", "Web"], required=True)

class Availability(Model):
    Provider = StringType(required=True)
    OfferType = StringType(choices=["Buy", "Subscription", "Free", "Rent"], required=True)
    DeliveryMethod = StringType(choices=["Scheduled", "Ticket", "Physical", "OnDemand"], required=True)
    Price = DecimalType(required=True)
    Currency = StringType(choices=["USD","GBP","EUR","JPY","CAD","SEK","DKK","CHF","AUD",
                                        "NZD","MXN","BRL","INR","NOK","THB","MYR","PHP","IDR",
                                        "RUB","SGD","KRW","ZAR","LTL","RON","BGN","HUF","CHE",
                                        "VEF","HKD","TWD","VND","COP"], required=True)
    PreSale = BooleanType(required=True)
    Quality = StringType(choices=["NOT_SET", "HD", "UHD", "SD"], required=True)
    Links = ListType(ModelType(OfferLink), required=True)
    Country = StringType(required=True)
    StartDate = DateTimeType()
    ExpirationDate = DateTimeType()
    Modifier = IntType(required=True)
    Modified = DateTimeType(required=True)

class ProviderMovie(Model):
    Title = StringType(required=True)
    Actors = ListType(StringType)
    Directors = ListType(StringType)
    Crew = ListType(StringType)
    Year = IntType()
    Source = StringType(max_length=100, required=True)
    PosterUrl = StringType(max_length=200)
    Description = StringType(max_length=2000)
    ProviderUniqueId = StringType(max_length=250, required=True)
    IMDBId = IMDbId()
    Url = StringType(required=True)
    Country = StringType()
    TMDBId = LongType()
    Availabilities = ListType(ModelType(Availability), required=True)
    Modifier = IntType(required=True)
    Modified = DateTimeType(required=True)
    Languages = StringType()

class ProviderEpisode(Model):
    Title = StringType(required=True)
    EpisodeNumber = IntType()
    Actors = ListType(StringType)
    Directors = ListType(StringType)
    Crew = ListType(StringType)
    Year = IntType()
    Source = StringType(max_length=100, required=True)
    ImageUrl = StringType(max_length=200)
    Description = StringType(max_length=2000)
    ProviderUniqueId = StringType(max_length=250, required=True)
    IMDBId = IMDbId()
    Url = StringType(required=True)
    Country = StringType()
    TMDBId = LongType()
    Availabilities = ListType(ModelType(Availability), required=True)
    Modifier = IntType(required=True)
    Modified = DateTimeType(required=True)

class ProviderSeason(Model):
    Title = StringType(required=True)
    SeasonNumber = IntType()
    Actors = ListType(StringType)
    Directors = ListType(StringType)
    Crew = ListType(StringType)
    Year = IntType()
    Source = StringType(max_length=100, required=True)
    PosterUrl = StringType(max_length=200)
    Description = StringType(max_length=2000)
    ProviderUniqueId = StringType(max_length=250, required=True)
    ProviderUniqueShowId = StringType(required=True)
    IMDBId = IMDbId()
    Url = StringType(required=True)
    Country = StringType()
    TMDBId = LongType()
    Availabilities = ListType(ModelType(Availability))
    Episodes = ListType(ModelType(ProviderEpisode))
    Modifier = IntType(required=True)
    Modified = DateTimeType(required=True)

class ProviderShow(Model):
    Title = StringType(required=True)
    Actors = ListType(StringType)
    Directors = ListType(StringType)
    Crew = ListType(StringType)
    Year = IntType()
    Source = StringType(max_length=100, required=True)
    PosterUrl = StringType(max_length=200)
    Description = StringType(max_length=2000)
    ProviderUniqueId = StringType(max_length=250, required=True)
    IMDBId = IMDbId()
    Url = StringType(required=True)
    Country = StringType()
    TMDBId = LongType()
    Availabilities = ListType(ModelType(Availability), required=True)
    Seasons = ListType(ModelType(ProviderSeason))
    Modifier = IntType(required=True)
    Modified = DateTimeType(required=True)
    Languages = StringType()

