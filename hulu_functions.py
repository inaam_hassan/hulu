import datetime

def link_gen(Link):
    """
    This function is used to join the url
    """

    URL = ''
    for i in Link:
        if 'https' in i.strip():
            URL = i
    return URL

def movies_offers(json_response):
    """
    This function is responsible to pick up offers list for movies.
    The return is a list of offers.
    """
    OfferLinksDict = []

    for links in json_response['potentialAction']['target']:
        type = ''
        link = links['actionPlatform']
        if 'Web' in link:
            type = 'Web'
        elif 'Android' in link:
            type = 'Android'
        elif 'IOS' in link:
            type = 'IOS'
        else:
            type = 'NOT_SET'
        OfferLinks = {
            'Url': str(links['urlTemplate']),
            'Platform': str(type)

        }
        OfferLinksDict.append(OfferLinks)

    return OfferLinksDict

def movies_availabilites(json_response,OfferLinksDict):
    """
    This function is responsible to pick up Availabilities list for movies.
    The return is a list of Availabilities.
    """
    Availabilities = []

    for avails in json_response['potentialAction']['expectsAcceptanceOf']:
        availType = ''
        if 'subscription' in avails['category'] or 'Subscription' in avails['category']:
            availType = 'Subscription'
        Available = {
            'Provider': str(avails['name']),
            'OfferType': str(availType),
            'DeliveryMethod': str('Physical'),
            'Price': float(avails['price']),
            'Currency': str(avails['priceCurrency']),
            'PreSale': False,
            'Quality': str('NOT_SET'),
            'Links': OfferLinksDict,
            'Country': str(avails['eligibleRegion']['name']),
            'StartDate': avails['availabilityStarts'],
            'ExpirationDate': avails['availabilityEnds'],
            'Modifier': 1,
            'Modified': datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z"

        }
        Availabilities.append(Available)

    return Availabilities

def movies(json_response,json_response2,Availabilities):
    """
    This function is responsible to gather all the movies.
    The return is a list of movies.
    """
    year = int(str(json_response['releasedEvent']['startDate']).split('-')[0])

    Movies = {
        'Title': json_response['name'],
        'Actors': None,
        'Directors': None,
        'Crew': None,
        'Year': year,
        'Description': json_response['description'],
        'Source': str('HULU'),
        'PosterUrl': json_response['image'],
        'ProviderUniqueId': str(json_response2['props']['pageProps']['query']['id']),
        'IMDBId': '',
        'Url': json_response['url'],
        'Country': json_response['releasedEvent']['location']['name'],
        # 'TMDBId': '',
        'Availabilities': Availabilities,
        'Modifier': 1,
        'Modified': datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z",
        'Languages': 'en-US',
    }

    return Movies


def show_offers(json_response):
    """
    This function is responsible to pick up offers list for TV series.
    The return is a list of offers.
    """
    OfferLinksDict = []

    for links in json_response['potentialAction']['target']:
        type = ''
        link = links['actionPlatform']
        if 'Web' in link:
            type = 'Web'
        elif 'Android' in link:
            type = 'Android'
        elif 'IOS' in link:
            type = 'IOS'
        else:
            type = 'NOT_SET'
        OfferLinks = {
            'Url': str(links['url']),
            'Platform': str(type)

        }

        OfferLinksDict.append(OfferLinks)

    return OfferLinksDict



def show_availabilities(json_response,OfferLinksDict):
    """
    This function is responsible to pick up Availabilities list for series.
    The return is a list of Availabilities.
    """
    Availabilities = []

    for avails in json_response['potentialAction']['expectsAcceptanceOf']:
        availType = ''
        if 'subscription' in avails['category'] or 'Subscription' in avails['category']:
            availType = 'Subscription'
        Availabe = {
            'Provider': str(avails['name']),
            'OfferType': str(availType),
            'DeliveryMethod': str('Physical'),
            'Price': float(avails['price']),
            'Currency': str(avails['priceCurrency']),
            'PreSale': False,
            'Quality': str('NOT_SET'),
            'Links': OfferLinksDict,
            'Country': str(avails['eligibleRegion']['name']),
            'StartDate': avails['availabilityStarts'],
            'ExpirationDate': avails['availabilityEnds'],
            'Modifier': 1,
            'Modified': datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z"

        }
        Availabilities.append(Availabe)
    return Availabilities


def unique_seasons(epss):
    """
    This function is responsible to make a unique list of seasons available.
    The return is a list of unique seasons.
    """
    SeasonListUnique = []
    for i in epss:
        val = i['season']
        if val not in SeasonListUnique:
            SeasonListUnique.append(val)
    return SeasonListUnique


def get_seasons(SeasonListUnique,json_response, epss, response, Availabilities):
    """
    This function is responsible to pick up all the seasons.
    The return is a list of seasons.
    """
    SeasonList = []
    for vals in SeasonListUnique:
        title = 'Season' + str(vals)
        Seasons = {
            'Title': title,
            'SeasonNumber': vals,
            'Actors': None,
            'Directors': None,
            'Crew': None,
            'Year': 0,
            'Source': str('HULU'),
            'PosterUrl': '',
            'Description': '',
            'ProviderUniqueId': '',
            'ProviderUniqueShowId': '',
            'IMDBId': '',
            'Url': json_response['url'],
            'Country': json_response['releasedEvent']['location']['name'],
            # 'TMDBId': None,
            'Availabilities': None,
            'Episodes': eps_func(epss, response, vals, Availabilities),
            'Modifier': 1,
            'Modified': datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z"
        }
        SeasonList.append(Seasons)
    return SeasonList

def eps_func(epss,response,val,Availabilities):
    """
    This function is responsible to pick up Episode list for series.
    The return is a list of Episodes.
    """


    Episodes = []
    for eps in epss:

        imageUrl = ''
        try:
            imageUrl = eps['artwork']['horizontalHero']['path']
        except:
            imageUrl = 'https:/hulu.com'
        if eps['season'] == val:
            year = int(str(eps['premiereDate']).split('-')[0])
            eps_dict = {

                'Title': eps['name'],
                'EpisodeNumber': eps['number'],
                'Actors': None,
                'Directors': None,
                'Crew': None,
                'Year': year,
                'Source': link_gen(response.css('link::attr(href)').extract()),
                'ImageUrl': imageUrl,
                'Description': eps['description'],
                'ProviderUniqueId': str(eps['id']),
                'IMDBId': '',
                'Url': link_gen(response.css('link::attr(href)').extract()),
                'Country': '',
                #'TMBDId': None,
                'Availabilities': Availabilities,
                'Modifier': 1,
                'Modified': datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z"
            }
            Episodes.append(eps_dict)
    return Episodes

def shows(json_response, json_response2, Availabilities, SeasonList):
    """
    This function is responsible to gather all TV series.
    The return is a list of TV series.
    """
    year = int(str(json_response['releasedEvent']['startDate']).split('-')[0])

    Show = {
        'Title': json_response['name'],
        'Actors': None,
        'Directors': None,
        'Crew': None,
        'Year': year,
        'Description': json_response['description'],
        'Source': str('HULU'),
        'PosterUrl': json_response['image'],
        'ProviderUniqueId': str(json_response2['props']['pageProps']['query']['id']),
        'IMDBId': '',
        'Url': json_response['url'],
        'Country': json_response['releasedEvent']['location']['name'],
        # 'TMDBId': None,
        'Availabilities': Availabilities,
        'Seasons': SeasonList,
        'Modifier': 1,
        'Modified': datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:23] + "Z",
        'Languages': 'en-US',
    }
    return Show
