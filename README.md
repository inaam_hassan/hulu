# Hulu Crawler
Example of Availabilities Extraction Crawler

## SETUP: "crawler" Directory
1. Create directory "C:\crawler".
2. Place "proxies.txt" file in "C:\crawler\proxies.txt".
3. Create directory "C:\crawler\output".
4. Create ProviderShow directory "C:\crawler\output\ProviderShow".
5. Create ProviderMovie directory "C:\crawler\output\ProviderMovie".
6. For EACH Provider, create a directory inside ***both*** "ProviderShow" and "ProviderMovie" folders named for the Provider.
    - This example ***requires*** a folder called "HULU" to be put inside "ProviderShow" and "ProviderMovie" directories. 

**NOTE**: These directories can be changed in the "settings.py" and "pipelines.py" files.

## Availability Model
-	Provider: String representing the source of the availabilities.
-	OfferType: type of financial requirement to view content
    - *Buy*: available to be purchased from provider
    - *Subscription*: requires subscription to service to see content
    - *Free*: can be viewed for free (often ad-supported)
    - *Rent*: available for rent from provider
-	DeliveryMethod: method for content delivery to viewer
    - *Scheduled*: linear content scheduled on TV
    - *Ticket*: content delivered in theaters
    - *Physical*: purchase of physical content (Bluray, etc.)
    - *OnDemand*: content available on-demand through streaming online or in app
-	PreSale: bool representing early sales of tickets (usually False)
-	StartDate: represents the date that the content became available for view on the service
-	ExpirationDate: represents the date that the content will expire off of the service
-	Modifier: ID representing the modifier in the database (to be provided)
-	Modified: datetime representing when the content was extracted


## Operating System:
-> Windows OS 10 

## Commands:
-> You need to redirect yourself to the spider folder
* in this case "cd HULU"  *
-> You need to run the scrapper with the bot name
* in this case "scrappy crawl HULUBOT"  *
